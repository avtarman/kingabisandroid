package com.example.brettmacdonald.kingabis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.List;

public class MenuListAdapter extends ArrayAdapter<Item>
{
    private static final String TAG = "MenuListAdapter";

    private Context mContext;
    private int mResource;

    /**
     * Default constructor for the MenuListAdapter
     * @param context
     * @param resource
     * @param objects
     */
    public MenuListAdapter(Context context, int resource, List objects)
    {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        /** get the item information */
        Item item = getItem(position);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView name = convertView.findViewById(R.id.name);
        TextView price = convertView.findViewById(R.id.price);
        TextView type = convertView.findViewById(R.id.type);
        TextView count = convertView.findViewById(R.id.count);

        name.setText(item.getName());
        NumberFormat format = NumberFormat.getCurrencyInstance();
        price.setText(format.format(item.getPrice()));
        type.setText(item.getType());
        count.setText("" + item.getCount());

        return convertView;
    }

    public Item retItem(int position, View convertView, ViewGroup parent)
    {
        return getItem(position);
    }
}



