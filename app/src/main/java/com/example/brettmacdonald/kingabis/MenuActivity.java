package com.example.brettmacdonald.kingabis;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MenuActivity extends MainActivity
{
    private ListView list_View;
    private ArrayList<Item> list = new ArrayList<>();
    private MenuListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        System.out.println("Menu Screen: Created");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        list_View = findViewById(R.id.list_view);

        /** hide add item button if the user role is a customer */
        if (user.getRole().equals("customer"))
        {
            Button hideAddButton = findViewById(R.id.addItemButton);
            hideAddButton.setVisibility(View.INVISIBLE);
        }
        /** hide view order button if the user role is employee */
        else if (user.getRole().equals("employee"))
        {
            Button hideOrderButton = findViewById(R.id.viewOrderButton);
            hideOrderButton.setVisibility(View.INVISIBLE);
        }

        /** get all items and populate the menu screen */
        adapter = new MenuListAdapter(MenuActivity.this, R.layout.adapter_view_layout, list);
        list_View.setAdapter(adapter);

        getAllItems();
        listViewListener();
    }

    /** Get all items from the server and populate menu */
    void getAllItems()
    {
        System.out.println("Menu Screen: Requesting All Items From Server");
        /** Send the login request to the server */
        client.get(url + "Items" + user.getAccessToken(), new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response)
            {
                System.out.println("Menu Screen: Successfully Grabbed All Items");
                // called when response HTTP status is "200 OK"
                try
                {
                    JSONArray res = new JSONArray(new String(response));
                    //ListView itemList = findViewById(R.id.list_view);
                    /** iterate through all of the items in the server response, add them to the layout one by one */
                    for(int i = 0; i < res.length(); i++)
                    {
                        Item item = new Item(new JSONObject(res.get(i).toString()));
                        System.out.println(res.get(i).toString());
                        list.add(item);
                        adapter.notifyDataSetChanged();
                    }
                }
                catch (Throwable t)
                {
                    System.out.println("Throw error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e)
            {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                requestFailedAlert(statusCode);
            }
        });
    }

    /**
     * when adding all the items to the menu view, add order/deletion functionality to each item based on user role
     */
    void listViewListener()
    {
        /** add and delete situations */
        list_View.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(final AdapterView<?> adapterView, View view, final int pos, long l)
            {
                if (user.getRole().equals("employee"))
                {
                    AlertDialog.Builder adb = new AlertDialog.Builder(MenuActivity.this);
                    adb.setTitle("Do You Want To Modify Item?");
                    adb.setMessage("" + adapter.retItem(pos, list_View, adapterView).getName());
                    adb.setNegativeButton("Cancel", null);
                    adb.setNeutralButton("Delete", new AlertDialog.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            AlertDialog.Builder adb2 = new AlertDialog.Builder(MenuActivity.this);
                            adb2.setTitle("Delete Item From Menu");
                            adb2.setMessage("Are you sure you want to delete " + adapter.retItem(pos, list_View, adapterView).getName() + "?");
                            adb2.setNegativeButton("Cancel", null);
                            adb2.setPositiveButton("Delete", new AlertDialog.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    deleteItem(adapter.retItem(pos, list_View, adapterView), pos);
                                }
                            });
                            adb2.show();
                        }
                    });
                    adb.setPositiveButton("Modify", new AlertDialog.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            modifyItemButton(list_View, adapter.retItem(pos, list_View, adapterView));
                        }
                    });
                    adb.show();
                }
                /** customers can add items to their order by clicking items in the menu */
                else if (user.getRole().equals("customer"))
                {
                    LinearLayout layout = new LinearLayout(MenuActivity.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    final EditText addItemCount = new EditText(MenuActivity.this);
                    addItemCount.setHint("Item Stock: Default value 1");
                    addItemCount.setInputType(InputType.TYPE_CLASS_NUMBER);
                    layout.addView(addItemCount);

                    AlertDialog.Builder adb = new AlertDialog.Builder(MenuActivity.this);
                    adb.setTitle("Add Item To Order?");
                    //adb.setView(layout);
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Add to Order", new AlertDialog.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            RequestParams params = new RequestParams();
                            int count = 1;
                            try {
                                count = Integer.parseInt(String.valueOf(addItemCount.getText()));
                            } catch (Throwable t) {
                                System.out.println("Throw error");
                            }
                            params.put("count", count);

                            orderItem(params, adapter.retItem(pos, list_View, adapterView));
                        }
                    });
                    adb.show();
                }
            }
        });
    }

    /**
     * employee makes a request to the server to delete an item
     */
    void deleteItem(Item item, final int pos)
    {
        System.out.println("Deleting item from database");
        /** send delete request */
        client.delete(url + "Items/" + item.getId() + user.getAccessToken(), new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response)
            {
                // called when response HTTP status is "200 OK"
                try
                {
                    System.out.println("Successfully deleted item from database");
                    /** if the item is successfully deleted, remove it from the menu right away */
                    list.remove(pos);
                    adapter.notifyDataSetChanged();
                } catch (Throwable t) {
                    System.out.println("Throw error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                requestFailedAlert(statusCode);
                System.out.println(new String(errorResponse));
            }
        });
    }

    /**
     * Add Item Button Response for employees
     */
    void addItemButton(View view)
    {
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        String addItemTitle = "Add Item";
        /** make a text entry slot so employee can enter item info */
        final EditText addItemName = new EditText(this);
        addItemName.setHint("Name");
        layout.addView(addItemName);

        final EditText addItemType = new EditText(this);
        addItemType.setHint("Type");
        layout.addView(addItemType);

        final EditText addItemPrice = new EditText(this);
        addItemPrice.setHint("Price");
        addItemPrice.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        layout.addView(addItemPrice);

        final EditText addItemCount = new EditText(this);
        addItemCount.setHint("Item Stock");
        addItemCount.setInputType(InputType.TYPE_CLASS_NUMBER);
        layout.addView(addItemCount);


        /** Show a pop up dialog telling the username/password is wrong */
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuActivity.this);
        builder.setTitle(addItemTitle)
                .setView(layout);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                // User clicked OK button
                /** create a new item based on the text input */
                RequestParams params = new RequestParams();
                params.put("name", String.valueOf(addItemName.getText()));
                params.put("type", String.valueOf(addItemType.getText()));
                params.put("price", Double.parseDouble(String.valueOf(addItemPrice.getText())));
                params.put("count", Integer.parseInt(String.valueOf(addItemCount.getText())));
                addItem(params);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * employee makes a request to the server to create an item
     */
    void addItem(RequestParams params)
    {
        /** params include item name, type, price and count */
        client.post(url + "Items" + user.getAccessToken(), params, new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response)
            {
                // called when response HTTP status is "200 OK"
                try
                {
                    System.out.println("Successfully added item to database");
                    /** add the new item and update the menu right away */
                    JSONObject res = new JSONObject(new String(response));
                    Item item = new Item(res);
                    list.add(item);
                    adapter.notifyDataSetChanged();
                } catch (Throwable t) {
                    System.out.println("Throw error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                requestFailedAlert(statusCode);
                System.out.println(new String(errorResponse));
            }
        });
    }

    /**
     * Modify Item Button Response for employees
     */
    void modifyItemButton(View view, final Item item)
    {
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        String addItemTitle = "Modify Item";
        /** make a text entry slot so employee can enter item info */
        final EditText addItemName = new EditText(this);
        addItemName.setText(item.getName());
        layout.addView(addItemName);

        final EditText addItemType = new EditText(this);
        addItemType.setText(item.getType());
        layout.addView(addItemType);

        final EditText addItemPrice = new EditText(this);
        addItemPrice.setText(item.getPrice().toString());
        addItemPrice.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        layout.addView(addItemPrice);

        final EditText addItemCount = new EditText(this);
        addItemCount.setText(item.getCount().toString());
        addItemCount.setInputType(InputType.TYPE_CLASS_NUMBER);
        layout.addView(addItemCount);


        /** Show a pop up dialog telling the username/password is wrong */
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuActivity.this);
        builder.setTitle(addItemTitle)
                .setView(layout);
        builder.setPositiveButton("Save Changes", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                // User clicked OK button
                /** create a new item based on the text input */
                RequestParams params = new RequestParams();
                params.put("name", String.valueOf(addItemName.getText()));
                params.put("type", String.valueOf(addItemType.getText()));
                params.put("price", Double.parseDouble(String.valueOf(addItemPrice.getText())));
                params.put("count", Integer.parseInt(String.valueOf(addItemCount.getText())));
                modifyItem(params, item);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * employee makes a request to the server to modify an item
     */
    void modifyItem(final RequestParams params, final Item item)
    {
        /** params include item name, type, price and count */
        client.patch(url + "Items/" + item.getId() + user.getAccessToken(), params, new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response)
            {
                // called when response HTTP status is "200 OK"
                try
                {
                    System.out.println("Successfully modified item to database");
                    /** add the new item and update the menu right away */
                    try
                    {
                        JSONObject res = new JSONObject(new String(response));
                        System.out.println(res);
                        try
                        {
                            item.setName(res.get("name").toString());
                            item.setType(res.get("type").toString());
                            item.setPrice(Double.parseDouble(res.get("price").toString()));
                            item.setCount(Integer.parseInt(res.get("count").toString()));
                        }
                        catch (Throwable t)
                        {
                            System.out.println(t);
                        }
                        adapter.notifyDataSetChanged();
                    }
                    catch (Throwable t)
                    {
                        System.out.println(t);
                    }

                    adapter.notifyDataSetChanged();
                } catch (Throwable t) {
                    System.out.println("Throw error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                requestFailedAlert(statusCode);
                System.out.println(new String(errorResponse));
            }
        });
    }

    /**
     * customer can add an item from the menu to their order
     */
    void orderItem(final RequestParams params, final Item item)
    {
        client.put(url + "Orders/" + user.getOrderId() + "/items/rel/" + item.getId() + user.getAccessToken(), params, new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response)
            {
                // called when response HTTP status is "200 OK"
                System.out.println("Successfully added item to order");
                try {
                    JSONObject res = new JSONObject(new String(response));
                    item.setCount(item.getCount() - Integer.parseInt(res.get("count").toString()));
                }
                catch (Throwable t)
                {
                    System.out.println(t);
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                requestFailedAlert(statusCode);
                System.out.println(new String(errorResponse));
            }
        });
    }


    /**
     * Logout Button Response
     */
    void logoutButton(View view)
    {
        /** send the logout request and switch back to the login screen */
        client.post(url + "Customers/logout" + user.getAccessToken(), new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                try
                {
                    System.out.println("Successfully logged out");
                    /** switch to main menu */
                    Intent intent = new Intent(MenuActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                catch (Throwable t)
                {
                    System.out.println("Throw error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                requestFailedAlert(statusCode);
                System.out.println(new String(errorResponse));
            }
        });
    }

    /**
     * View Order Screen Button Response for customers
     */
    void viewOrderButton(View view) {
        Intent intent = new Intent(MenuActivity.this, OrderActivity.class);
        startActivity(intent);
    }

    /** Create a popup dialog box to show the user the request failed */
    void requestFailedAlert(int statusCode)
    {
        String errTitle = "Request Failed";
        String errMsg = "Error code: " + statusCode;
        if (statusCode == 404)
        {
            errMsg = "Cannot connect to server. Please try again.";
        }
        else if (statusCode == 401)
        {
            errMsg = "Unauthorized request. You do not have the permissions to do this.";
        }

        /** Show a pop up dialog telling the username/password is wrong */
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuActivity.this);
        builder.setMessage(errMsg)
                .setTitle(errTitle);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
