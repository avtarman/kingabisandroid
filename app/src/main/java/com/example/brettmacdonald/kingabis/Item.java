package com.example.brettmacdonald.kingabis;

import org.json.JSONObject;

public class Item
{
    private String name, type, id;
    private Integer count;
    private Double price;

    public Item(JSONObject res)
    {
        try
        {
            this.name = res.get("name").toString();
            this.id = res.get("id").toString();
            this.price = Double.parseDouble(res.get("price").toString());
            this.count = Integer.parseInt(res.get("count").toString());
            this.type = res.get("type").toString();
        }
        catch (Throwable t)
        {
            System.out.println(t);
        }
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name) { this.name = name; }

    public Double getPrice() { return price; }

    public void setPrice(Double price) { this.price = price; }

    public String getType()
    {
        return type;
    }

    public void setType(String type) { this.type = type; }

    public Integer getCount()
    {
        return count;
    }

    public void setCount(Integer count) { this.count = count; }

    public String getId()
    {
        return id;
    }
}
