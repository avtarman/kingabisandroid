package com.example.brettmacdonald.kingabis;

import org.json.JSONObject;

public class Customer
{
    private String accessToken, id, username, role, orderId;

    public Customer(JSONObject res)
    {
        try
        {
            this.accessToken = "?access_token=" + res.get("id").toString();
            this.id = res.get("userId").toString();
            this.role = res.get("role").toString();
            this.username = res.get("username").toString();
            this.orderId = res.get("currentOrder").toString();
        }
        catch (Throwable t)
        {
            System.out.println(t);
        }
    }

    public String getUsername()
    {
        return username;
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public String getId()
    {
        return id;
    }

    public String getRole()
    {
        return role;
    }

    public String getOrderId()
    {
        return orderId;
    }
}
