package com.example.brettmacdonald.kingabis;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class OrderActivity extends MainActivity
{
    private ListView list_View;
    private ArrayList<Item> list = new ArrayList<>();
    private MenuListAdapter adapter;
    private Double totalPrice = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        list_View = findViewById(R.id.list_view);

        adapter = new MenuListAdapter(this, R.layout.adapter_view_layout, list);
        list_View.setAdapter(adapter);

        getOrderItems();
        listViewListener();
    }

    /** Get all items for the current order from the server and populate order */
    void getOrderItems()
    {
        /** Send the order request to the server */
        client.get(url + "Orders/" + user.getOrderId() + "/items" + user.getAccessToken(), new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response)
            {
                // called when response HTTP status is "200 OK"
                try
                {
                    JSONArray res = new JSONArray(new String(response));
                    totalPrice = 0.0;
                    /** iterate through all of the items in the server response, add them to the layout one by one */
                    for(int i = 0; i < res.length(); i++)
                    {
                        Item item = new Item(new JSONObject(res.get(i).toString()));
                        totalPrice += item.getPrice();
                        list.add(item);
                        adapter.notifyDataSetChanged();
                    }
                }
                catch (Throwable t)
                {
                    System.out.println("Throw error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e)
            {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                requestFailedAlert(statusCode);
                System.out.println(new String(errorResponse));
            }
        });
    }

    /**
     * when adding all the items to the menu view, add order/deletion functionality to each item based on user role
     */
    void listViewListener()
    {
        /** add and delete situations */
        list_View.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(final AdapterView<?> adapterView, View view, final int pos, long l)
            {
                AlertDialog.Builder adb = new AlertDialog.Builder(OrderActivity.this);
                adb.setTitle("Do You Want To Remove Item From Order?");
                adb.setMessage("" + adapter.retItem(pos, list_View, adapterView).getName());
                adb.setNegativeButton("Cancel", null);
                adb.setPositiveButton("Remove", new AlertDialog.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        deleteItem(adapter.retItem(pos, list_View, adapterView), pos);
                    }
                });
                adb.show();
            }
        });
    }

    /** Logout Button Response */
    void logoutButton(View view)
    {
        /** send the logout request and switch back to the login screen */
        client.post(url + "Customers/logout" + user.getAccessToken(), new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response)
            {
                // called when response HTTP status is "200 OK"
                try
                {
                    System.out.println("Successfully logged out");
                    /** switch to main menu */
                    Intent intent = new Intent(OrderActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                catch (Throwable t)
                {
                    System.out.println("Throw error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e)
            {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                requestFailedAlert(statusCode);
                System.out.println(new String(errorResponse));
            }
        });
    }

    /** View Menu Screen Button Response for customers */
    void viewMenuButton(View view)
    {
        Intent intent = new Intent(OrderActivity.this, MenuActivity.class);
        startActivity(intent);
    }

    /** Checkout button for customers */
    void checkoutButton(final View view)
    {
        client.post(url + "Orders/" + user.getOrderId() + "/checkout/" + user.getAccessToken(), new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response)
            {
                // called when response HTTP status is "200 OK"
                try
                {
                    /** if item is removed from order, update the order ui right away */
                    AlertDialog.Builder adb = new AlertDialog.Builder(OrderActivity.this);
                    adb.setTitle("Do You Want To Checkout Your Order?");
                    NumberFormat format = NumberFormat.getCurrencyInstance();
                    adb.setMessage("Total Price: " + format.format(totalPrice));
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Pay and Logout", new AlertDialog.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            logoutButton(view);
                        }
                    });
                    adb.show();
                }
                catch (Throwable t)
                {
                    System.out.println("Throw error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e)
            {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                requestFailedAlert(statusCode);
                System.out.println(new String(errorResponse));
            }
        });
    }

    /** sends the request to the server to remove an item from the current order */
    void deleteItem(final Item item, final int pos)
    {
        client.delete(url + "Orders/" + user.getOrderId() + "/items/rel/" + item.getId() + user.getAccessToken(), new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response)
            {
                // called when response HTTP status is "200 OK"
                try
                {
                    System.out.println("Successfully deleted item to database");
                    /** if item is removed from order, update the order ui right away */
                    totalPrice -= item.getPrice();
                    list.remove(pos);
                    adapter.notifyDataSetChanged();
                }
                catch (Throwable t)
                {
                    System.out.println("Throw error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e)
            {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                requestFailedAlert(statusCode);
                System.out.println(new String(errorResponse));
            }
        });
    }

    /** Create a popup dialog box to show the user the request failed */
    void requestFailedAlert(int statusCode)
    {
        String errTitle = "Request Failed";
        String errMsg = "Error code: " + statusCode;
        if (statusCode == 404)
        {
            errMsg = "Cannot connect to server. Please try again.";
        }
        else if (statusCode == 401)
        {
            errMsg = "Unauthorized request. You do not have the permissions to do this.";
        }

        /** Show a pop up dialog telling the username/password is wrong */
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderActivity.this);
        builder.setMessage(errMsg)
                .setTitle(errTitle);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
